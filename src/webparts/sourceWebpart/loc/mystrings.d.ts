declare interface ISourceWebpartWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'SourceWebpartWebPartStrings' {
  const strings: ISourceWebpartWebPartStrings;
  export = strings;
}
