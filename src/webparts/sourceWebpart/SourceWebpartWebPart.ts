import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './SourceWebpartWebPart.module.scss';
import * as strings from 'SourceWebpartWebPartStrings';

import { IDynamicDataCallables, IDynamicDataPropertyDefinition } from '@microsoft/sp-dynamic-data';

export interface IPoint { //定义IPoint接口
  x: number;
  y: number;
}

export interface ISourceWebpartWebPartProps {
  description: string;
}

export default class SourceWebpartWebPart extends BaseClientSideWebPart<ISourceWebpartWebPartProps>
                                          implements IDynamicDataCallables
{  
  private mousePosition: IPoint;  //mousePosition用来保存鼠标位置数据

  protected onInit(): Promise<void> {
    //将webpart注册为动态数据源
    this.context.dynamicDataSourceManager.initializeSource(this);

    return Promise.resolve();
  }

  public onMouseClick(e) {  //onMouseClick方法用来获取鼠标位置数据并保存在mousePosition中
    this.mousePosition = { x: e.clientX, y: e.clientY };
    this.context.dynamicDataSourceManager.notifyPropertyChanged('x');
    this.context.dynamicDataSourceManager.notifyPropertyChanged('y');
  }

  public getPropertyDefinitions(): ReadonlyArray<IDynamicDataPropertyDefinition> {
    return [
      {
        id: 'x',
        title: 'Mouse-X'
      },
      {
        id: 'y',
        title: 'Mouse-y'
      }
    ];
  } 

  public getPropertyValue(propertyId: string): number {
    switch (propertyId) {
      case 'x':
        return this.mousePosition.x;
      case 'y':
        return this.mousePosition.y;
    }

    throw new Error('Bad property id');
  }
  
  public render(): void {
    //显示黄色区域
    this.domElement.innerHTML = `
      <div id="webpartdiv" style="width: 700px; height: 200px; background-color: yellow">
        
      </div>`;

    //绑定onclick事件
    this.domElement.onclick=this.onMouseClick.bind(this);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: 'Description',
              groupFields: [
                PropertyPaneTextField('description', {
                  label: 'description'
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
