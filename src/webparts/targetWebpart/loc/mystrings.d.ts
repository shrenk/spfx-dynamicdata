declare interface ITargetWebpartWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'TargetWebpartWebPartStrings' {
  const strings: ITargetWebpartWebPartStrings;
  export = strings;
}
