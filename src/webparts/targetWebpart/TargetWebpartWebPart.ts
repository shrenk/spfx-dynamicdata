import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField,
  IWebPartPropertiesMetadata,
  IPropertyPaneConditionalGroup,
  DynamicDataSharedDepth,
  PropertyPaneDynamicFieldSet,
  PropertyPaneDynamicField
} from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './TargetWebpartWebPart.module.scss';
import * as strings from 'TargetWebpartWebPartStrings';

import { DynamicProperty } from '@microsoft/sp-component-base';

export interface ITargetWebpartWebPartProps {
  description: string;
  x: DynamicProperty<number>;
  y: DynamicProperty<number>;
}

export default class TargetWebpartWebPart extends BaseClientSideWebPart<ITargetWebpartWebPartProps> {

  public render(): void {
    
    const x: number | undefined = this.properties.x.tryGetValue();
    const y: number | undefined = this.properties.y.tryGetValue();
    console.log(x);
    this.domElement.innerHTML = `
      <div class="${ styles.targetWebpart }">
        <div class="${ styles.container }">
          <div class="${ styles.row }">
            <div class="${ styles.column }">
              <span class="${ styles.title }">TargetWebpart</span>
              <p class="${ styles.subTitle }">显示鼠标x和y坐标</p>
                <div>Mouse X: ${ x == undefined ? '0' : x }</div>
                <div>Mouse Y: ${ y == undefined ? '0' : y }</div>
            </div>
          </div>
        </div>
      </div>`;
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected get propertiesMetadata(): IWebPartPropertiesMetadata {
    return {
      'x': {
        dynamicPropertyType: 'number'
      },
      'y': {
        dynamicPropertyType: 'number'
      }
    };
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          groups: [
            {
              groupFields: [
                PropertyPaneDynamicFieldSet({
                  label: 'Select data source',
                  fields: [
                    PropertyPaneDynamicField('x', {
                      label: 'Position x'
                    })
                  ]
                }),
                PropertyPaneDynamicFieldSet({
                  label: 'Select data source',
                  fields: [
                    PropertyPaneDynamicField('y', {
                      label: 'Position y'
                    })
                  ]
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
